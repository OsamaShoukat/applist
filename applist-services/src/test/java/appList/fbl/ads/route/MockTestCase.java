// package adservicescom.fbl.ads.route;

// import javax.enterprise.context.ApplicationScoped;
// import javax.inject.Inject;


// import org.junit.jupiter.api.Assertions;
// import org.junit.jupiter.api.BeforeAll;
// import org.junit.jupiter.api.Test;
// import org.mockito.Mockito;

// import io.quarkus.test.junit.QuarkusMock;
// import io.quarkus.test.junit.QuarkusTest;

// @QuarkusTest
// public class MockTestCase {

//     @Inject
//     MockableBean1 mockableBean1;

//     @Inject
//     MockableBean2 mockableBean2;

//     @BeforeAll
//     public static void setup() {
//         MockableBean1 mock = Mockito.mock(MockableBean1.class);  
//         System.out.println("mock altaf");
//         Mockito.when(mock.greet("Stuart")).thenReturn("A mock for Stuart");
//         QuarkusMock.installMockForType(mock, MockableBean1.class);  
//     }

//     @Test
//     public void testBeforeAll() {
//     	System.out.println("mock altaf a");
//         Assertions.assertEquals("A mock for Stuart", mockableBean1.greet("Stuart"));  
//         Assertions.assertEquals("Hello Stuart", mockableBean2.greet("Stuart")); 
//     }

//     @Test
//     public void testPerTestMock() {
//     	System.out.println("mock altaf b");
//         QuarkusMock.installMockForInstance(new BonjourMockableBean2(), mockableBean2); 
//         Assertions.assertEquals("A mock for Stuart", mockableBean1.greet("Stuart"));  
//         Assertions.assertEquals("Bonjour Stuart", mockableBean2.greet("Stuart")); 
//     }

//     @ApplicationScoped
//     public static class MockableBean1 {

//         public String greet(String name) {
//             return "Hello " + name;
//         }
//     }

//     @ApplicationScoped
//     public static class MockableBean2 {

//         public String greet(String name) {
//             return "Hello " + name;
//         }
//     }

//     public static class BonjourMockableBean2 extends MockableBean2 {
//         @Override
//         public String greet(String name) {
//             return "Bonjour " + name;
//         }
//     }
// }