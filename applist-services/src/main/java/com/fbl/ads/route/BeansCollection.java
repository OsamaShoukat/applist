package com.fbl.ads.route;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.camel.Exchange;
import org.jboss.logging.Logger;

import com.fbl.ads.helper.IntegrationHelperBean;
import com.fbl.ads.types.AppType;
import com.fbl.ads.types.GetAppDetailsResponseType;
import com.fbl.ads.types.GetAppResponseDetailsType;
import com.fbl.ads.types.ResponseHeaderType;
import io.agroal.api.AgroalDataSource;
import io.quarkus.agroal.DataSource;
import io.quarkus.runtime.annotations.RegisterForReflection;

@ApplicationScoped
@Named("beansCollection")
@RegisterForReflection
public class BeansCollection {

  @Inject
  @DataSource("kogito")
  AgroalDataSource kogitoDataSource;
  
  //kogitoDataSource
  Connection conn = null;
  CallableStatement cstmt = null;

  private static final Logger LOG = Logger.getLogger(BeansCollection.class);

  public void get_appcentre_apps(Exchange exchange) throws SQLException {
    String invalidParametersFlag = "N";
    String msg = "";

    List<AppType> appList = new ArrayList<AppType>();

    try {
      conn = kogitoDataSource.getConnection();

     

       cstmt = conn.prepareCall("{ call get_appcentre_apps(?,?,?,?,?,?,?) }");
      cstmt.registerOutParameter(1, java.sql.Types.ARRAY, "CHAR_COL_TYPE");
      cstmt.registerOutParameter(2, java.sql.Types.ARRAY, "CHAR_COL_TYPE");
      cstmt.registerOutParameter(3, java.sql.Types.ARRAY, "CHAR_COL_TYPE");
      cstmt.registerOutParameter(4, java.sql.Types.ARRAY, "CHAR_COL_TYPE");
      cstmt.registerOutParameter(5, java.sql.Types.ARRAY, "CHAR_COL_TYPE");
      cstmt.registerOutParameter(6, java.sql.Types.ARRAY, "CHAR_COL_TYPE");
      cstmt.registerOutParameter(7, java.sql.Types.ARRAY, "CHAR_COL_TYPE");

      cstmt.executeUpdate();

      Array appIdArray = cstmt.getArray(1);
      Array appShortNameArray = cstmt.getArray(2);
      Array appNameArray = cstmt.getArray(3);
      Array appIconNameArray = cstmt.getArray(4);
      Array appResourcePathArray = cstmt.getArray(5);
      Array appBundleNameArray = cstmt.getArray(6);
      Array appVersionArray = cstmt.getArray(7);

      List<String> appIdList = Arrays.asList((String[]) appIdArray.getArray());
      List<String> appShortNameList = Arrays.asList((String[]) appShortNameArray.getArray());
      List<String> appNameList = Arrays.asList((String[]) appNameArray.getArray());
      List<String> appIconNameList = Arrays.asList((String[]) appIconNameArray.getArray());
      List<String> appResourcePathappIconNameList = Arrays.asList((String[]) appResourcePathArray.getArray());
      List<String> appBundleNameappIconNameList = Arrays.asList((String[]) appBundleNameArray.getArray());
      List<String> appVersionappIconNameList = Arrays.asList((String[]) appVersionArray.getArray());

      for (int i = 0; i < appIdList.size(); i++) {
        AppType app = new AppType();

        app.setAppId(appIdList.get(i));
        app.setAppShortName(appShortNameList.get(i));
        app.setAppName(appNameList.get(i));
        app.setAppIconName(appIconNameList.get(i));
        app.setAppResourcePath(appResourcePathappIconNameList.get(i));
        app.setAppBundleName(appBundleNameappIconNameList.get(i));
        app.setAppVersion(appVersionappIconNameList.get(i));
        appList.add(app);

      }
      // List(AppType) appTypeList = ArrayList<AppType>;

      // for loop on appIdList {
      // create App object
      // set app object id property with appIdList(i)
      // set app object name property with appNameList(i)
      // and so on

      // when all properties are set
      // appTypeList.add(appObject)
      // }
      if (cstmt != null) {
        LOG.info("getAppListDetails| Statement Closed");
        cstmt.close();
      }

      if (conn != null) {
        LOG.info("getAppListDetails| Connection Closed");
        conn.close();
      }

    }

    catch (Exception e) {
      e.printStackTrace();
      invalidParametersFlag = "Y";
      msg = "There is Exception";
    }

    GetAppDetailsResponseType response = new GetAppDetailsResponseType();
    ResponseHeaderType responseHeader = new ResponseHeaderType();
    GetAppResponseDetailsType responseDetails = new GetAppResponseDetailsType();

    if (invalidParametersFlag.equalsIgnoreCase("N")) {

      responseHeader.setRequestReferenceId(exchange.getProperty("requestId").toString());
      responseHeader.setResponseReferenceId(IntegrationHelperBean.getUniqueReferenceId());
      responseHeader.setResponseDateTime(IntegrationHelperBean.getCurrentDateString("yyyy-MM-dd'T'HH:mm:ss'Z'"));
      responseHeader.setResponseCode(IntegrationHelperBean.PASS_CODE);
      responseHeader.setResponseMessage("Success");

      responseDetails.setAppList(appList);

      response.setResponseHeader(responseHeader);
      response.setResponseDetails(responseDetails);

    } else {

      responseHeader.setRequestReferenceId(exchange.getProperty("requestId").toString());
      responseHeader.setResponseReferenceId(IntegrationHelperBean.getUniqueReferenceId());
      responseHeader.setResponseDateTime(IntegrationHelperBean.getCurrentDateString("yyyy-MM-dd'T'HH:mm:ss'Z'"));
      responseHeader.setResponseCode(IntegrationHelperBean.INVALID_PARAMETERS_CODE);
      responseHeader.setResponseMessage(msg);

      response.setResponseHeader(responseHeader);
      // response.setResponseDetails(registrationDetails);
    }

    exchange.getIn().setBody(response, GetAppDetailsResponseType.class);

  }

}
