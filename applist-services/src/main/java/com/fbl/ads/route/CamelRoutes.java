package com.fbl.ads.route;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spi.PropertiesComponent;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.spi.ConfigSource;
import org.jboss.logging.Logger;
import org.apache.commons.lang3.StringUtils;

@ApplicationScoped
public class CamelRoutes extends RouteBuilder {

        private static final Logger LOG = Logger.getLogger(CamelRoutes.class);

        private void initializeCustomProperties(CamelContext context) {
                Iterable<ConfigSource> iterable = ConfigProvider.getConfig().getConfigSources();
                for (ConfigSource configSource : iterable) {
                        if (configSource.getName().contains("application.properties")) {
                                PropertiesComponent pc = context.getPropertiesComponent();
                                pc.setLocation(StringUtils.substring(configSource.getName(),
                                                StringUtils.indexOf(configSource.getName(), "file"), -1));
                                context.setPropertiesComponent(pc);

                        }
                }
        }

        @Override
        public void configure() throws Exception {
                initializeCustomProperties(getCamelContext());

                restConfiguration()
                                .component("servlet")
                                .contextPath("{{application.context.path}}")
                                .dataFormatProperty("prettyPrint", "true")
                                .bindingMode(RestBindingMode.off)
                                .enableCORS(true)
                                .corsAllowCredentials(true)
                                .corsHeaderProperty("Access-Control-Allow-Headers",
                                                "Authorization, Origin, Accept, X-Forwarded-For, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-Allow-Credentials")
                                .corsHeaderProperty("Access-Control-Allow-Credentials", "true")
                                .apiContextPath("{{application.api.context.path}}")
                                .apiContextPath("api-doc")
                                .apiProperty("api.title", "{{application.api.title}}")
                                .apiProperty("api.version", "{{application.api.version}}")
                                .apiProperty("cors", "true");

                rest("/health").id("health")
                                .produces("application/json")
                                .get().description("Heartbeat service").id("getHealth")

                                .to("direct:health");

                from("direct:health").routeId("healthRoute").streamCaching()
                                .log("Health route")
                                .setBody().constant("OK")
                                .marshal().json()

                                .end();

                ////////////////////////////////////// appList///////////////////////////////////////////////////
                ////////////////////////////////////// rest///////////////////////////////////////////////////////

                rest("/appList/all")
                                .produces("application/json")

                                .get()
                                .consumes("application/json")
                                // .param()
                                // .name("applicationId")
                                // .type(RestParamType.query)
                                // .description("Identifier of the application calling the service")
                                // .dataType("string")
                                // .required(true)
                                // .endParam()
                                .param()
                                .name("requestId")
                                .type(RestParamType.query)
                                .description("Unique identifier from channel for every call")
                                .dataType("string")
                                .required(true)
                                .endParam()
                                .responseMessage()
                                .code(200)
                                .message("Success")
                                .endResponseMessage()
                                .responseMessage()
                                .code(400)
                                .message("Invalid parameters")
                                .endResponseMessage()
                                .responseMessage()
                                .code(401)
                                .message("Authorization failure")
                                .endResponseMessage()
                                .responseMessage()
                                .code(404)
                                .message("Invalid REST call")
                                .endResponseMessage()
                                .responseMessage()
                                .code(504)
                                .message("Connection failure")
                                .endResponseMessage()
                                .to("direct:getAppList");

                from("direct:getAppList")
                                .setProperty("requestId").simple("${header.requestId}")
                                .log("Get AppList- Start")

                                .to("bean:beansCollection?method=get_appcentre_apps")

                                .marshal().json()

                                .log("Get AppList - End")
                                .end();

        }
}