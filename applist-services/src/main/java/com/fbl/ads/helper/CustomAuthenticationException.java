package com.fbl.ads.helper;

public class CustomAuthenticationException extends Exception {
  
  /**
	 *
	 */
	private static final long serialVersionUID = 1L;

public CustomAuthenticationException(String errorMessage) {
    super(errorMessage);
  }
}