package com.fbl.ads.helper;

public class HelperResponseType {
  protected String responseCode;
  protected String responseText;
  
  public String getResponseCode() {
    return responseCode;
  }
  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }
  
  public String getResponseText() {
    return responseText;
  }
  public void setResponseText(String responseText) {
    this.responseText = responseText;
  }  
  
  
}
