package com.fbl.ads.helper;

import java.math.BigDecimal;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jboss.logging.Logger;

import io.quarkus.runtime.annotations.RegisterForReflection;
import oracle.jdbc.driver.OracleConnection;


@ApplicationScoped
@Named("helperBean")
@RegisterForReflection
public class IntegrationHelperBean {
  
  private static final Logger LOG = Logger.getLogger(IntegrationHelperBean.class);
  
  public static final String PASS_CODE = "0";
  public static final String ERROR_CODE = "1";
  public static final String NO_DATA_FOUND_CODE = "2";
  public static final String DB_FAILURE_CODE = "3";
  public static final String DUPLICATE_CODE = "4"; 
  public static final String ROW_LOCKED_CODE = "5";
  public static final String INVALID_PARAMETERS_CODE = "400"; 
  public static final String AUTH_FAILURE_CODE = "401";
  public static final String CONN_FAILURE_CODE = "504";

  HelperResponseType helperResponse = new HelperResponseType();
  
  public static XMLGregorianCalendar getXMLGregorianDateCalendar(String dbDate) {
    XMLGregorianCalendar xgc = null;
    try {
      if ((dbDate != null) && (dbDate != "") && (dbDate != "null") && (dbDate != "NULL")) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = String.valueOf(dbDate);
        date = sdf.parse(formattedDate);
        GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
        gc.setTime(date);
        xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        xgc.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        xgc.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    
    return xgc;
  }
  
  public static XMLGregorianCalendar getXMLGregorianDateTimeCalendar(String dbDate) {
    XMLGregorianCalendar xgc = null;
    try {
      if ((dbDate != null) && (dbDate != "") && (dbDate != "null") && (dbDate != "NULL")) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String formattedDate = String.valueOf(dbDate);
        date = sdf.parse(formattedDate);
        GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
        gc.setTime(date);
        xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        xgc.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        xgc.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    
    return xgc;
  }
  
  public static XMLGregorianCalendar getXMLGregorianDateTimeZoneCalendar(String dbDate) {
    XMLGregorianCalendar xgc = null;
    try {
      if ((dbDate != null) && (dbDate != "") && (dbDate != "null") && (dbDate != "NULL")) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String formattedDate = String.valueOf(dbDate);
        date = sdf.parse(formattedDate);
        GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
        gc.setTime(date);
        xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        xgc.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        xgc.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    
    return xgc;
  }
  
  public static String getXMLDateString(XMLGregorianCalendar xmlDate) {
    String dateString = "";
    try {
      if ((xmlDate != null)) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        dateString = sdf.format(xmlDate.toGregorianCalendar().getTime());
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    
    return dateString;
  }
  
  public static String getXMLTimeString(XMLGregorianCalendar xmlDate) {
    String dateString = "";
    try {
      if ((xmlDate != null)) {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss", Locale.getDefault());
        dateString = sdf.format(xmlDate.toGregorianCalendar().getTime());
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    
    return dateString;
  }
  
  public static String getCurrentDateString(String stringToConvert) {
    Date today = Calendar.getInstance().getTime();
    
    Format formatter = new SimpleDateFormat(stringToConvert);
    String dateInString = formatter.format(today);
    
    return dateInString;
  }
  
  public static BigDecimal checkSetBigDecimalNull(BigDecimal bigDecimalValue) {
    return (bigDecimalValue != null ? bigDecimalValue : null);
  }
  
  public static Long checkSetLongNull(String longStringValue) {
    return (((longStringValue != null) && (longStringValue != "")) ? Long.valueOf(longStringValue) : null);
  }
  
  public static Long checkSetLongNull(BigDecimal bigDecimalValue) {
    return ((bigDecimalValue != null) ? Long.valueOf(bigDecimalValue.toString()) : null);
  }
  
  public static Integer checkSetIntegerNull(String intStringValue) {
    return (((intStringValue != null) && (intStringValue != ""))? Integer.valueOf(intStringValue) : null);
  }
  
  public static Integer checkSetIntegerNull(BigDecimal bigDecimalValue) {
    return ((bigDecimalValue != null) ? Integer.valueOf(bigDecimalValue.toString()) : null);
  }
  
  public static String checkSetStringNull(String stringValue) {
    return (stringValue != null ? stringValue : null);
  }

  public static String checkSetObjectNull(Object objectValue) {
    return (objectValue == null ? "" : String.valueOf(objectValue));
  }

  public static XMLGregorianCalendar checkSetXMLGregorianCalendarNull(XMLGregorianCalendar dateValue) {
    return (dateValue != null ? dateValue : null);
  }

  public static String getDecryptedValue(String encryptedValue, String key, Exchange exchange ) {
    String result = StringUtils.substringBetween(encryptedValue, "ENC(", ")");
    
    EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
    config.setPassword("FBL" + key);
    
    StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    encryptor.setConfig(config);
    
    return(encryptor.decrypt(result));
  }
  

  public static OracleConnection getDBConnection(String jdbcHost, String jdbcPort, String jdbcService, String username, String password, HelperResponseType helperResponse) throws Exception {
    helperResponse.setResponseCode(PASS_CODE);
    helperResponse.setResponseText("SUCCESS");
    
    OracleConnection dbConnection = null;
    Driver oracleDriver = null;
    String jdbcUrl = "jdbc:oracle:thin:@" + jdbcHost + ":" + jdbcPort + "/" + jdbcService;

    if (helperResponse.getResponseCode() == PASS_CODE) {
      try {
          Class.forName("oracle.jdbc.OracleDriver");
          
          if (oracleDriver == null) {
            oracleDriver = new oracle.jdbc.driver.OracleDriver();
          }
          
          DriverManager.registerDriver(oracleDriver);

          if (dbConnection == null) {
            dbConnection = (OracleConnection) DriverManager.getConnection(jdbcUrl, username, password);
          }
        
        if (dbConnection == null) {
              helperResponse.setResponseCode(ERROR_CODE);
              helperResponse.setResponseText("No Connection");
        }
        
        return dbConnection;
      } catch (SQLException e) {
        if (helperResponse.getResponseCode() == IntegrationHelperBean.PASS_CODE) {
          helperResponse.setResponseCode(IntegrationHelperBean.ERROR_CODE);
          helperResponse.setResponseText(ExceptionUtils.getStackTrace(e));
        }           
        LOG.info("IntegrationHelperBean.getDBConnection| Error in Connection| " + helperResponse.getResponseText());
      }
    }
    
    return dbConnection;
  } 

  public static String getUniqueReferenceId() {
    return StringUtils.rightPad(String.valueOf(IntegrationHelperBean.getCurrentDateString("yyDDD") + Integer.toHexString(UUID.randomUUID().hashCode())), 14, "0");
  }
  
  public void createHeaderMap(Exchange message) {
    Map<String, Object> hashMap = new HashMap<String, Object>();
    message.getIn().setHeader("HEADER_MAP", hashMap);
  }

/*  
  public static void setHeader(String headerName, Object headerValue, Exchange message) {
    Map<String, Object> hashMap = (Map<String, Object>)message.getIn().getHeader("HEADER_MAP");
    Object[] mapObject = {headerValue};
    hashMap.put(headerName, mapObject);
    
    message.getIn().setHeader("HEADER_MAP", hashMap);
  }
  
  public static Object getHeader(String headerName, Exchange message) {
    Map<String, Object> hashMap = (Map<String, Object>)message.getIn().getHeader("HEADER_MAP");
    Object[] mapObject = (Object[])hashMap.get(headerName);
    Object headerValue = mapObject[0];
    
    return headerValue;
  } 
*/
  
  public static byte[] encryptForBlob(byte[] plainData, String key, String initializationVector) {
    byte[] keyData = key.getBytes();
    byte[] ivData = initializationVector.getBytes();
    byte[] encryptedData = null;
    
    try {
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      SecretKeySpec myKey = new SecretKeySpec(keyData, "AES");
      IvParameterSpec ivspec = new IvParameterSpec(ivData);
      cipher.init(Cipher.ENCRYPT_MODE, myKey, ivspec);    
      encryptedData = cipher.doFinal(plainData);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return encryptedData;
  }
  
  public static String getLdapErrorDescription (String errorText) {
    String errorDescription = null;
    String searchString = "LDAP: error code";
    int iErrorCode = 999;
    if (StringUtils.indexOf(errorText, searchString, 0) >= 0) {
      int startPosition = StringUtils.indexOf(errorText, searchString, 0) + 17;
      iErrorCode = Integer.valueOf(StringUtils.trim(StringUtils.substring(errorText, startPosition, startPosition + 2)));
    } else {
      iErrorCode = 999;
    }
    switch (iErrorCode) {
      case 0: {
        errorDescription = "No error";
        break;
      }
      case 1: {
        errorDescription = "Operations error";
        break;
      }
      case 2: {
        errorDescription = "Protocol error";
        break;
      }
      case 3: {
        errorDescription = "Time limit exceeded";
        break;
      }
      case 4: {
        errorDescription = "Size limit exceeded";
        break;
      }
      case 5: {
        errorDescription = "Compared false";
        break;
      }
      case 6: {
        errorDescription = "Compared true";
        break;
      }
      case 7: {
        errorDescription = "Authentication method not supported";
        break;
      }
      case 8: {
        errorDescription = "Strong authentication required";
        break;
      }
      case 9: {
        errorDescription = "Partial results being returned";
        break;
      }
      case 10: {
        errorDescription = "Referral encountered";
        break;
      }
      case 11: {
        errorDescription = "Administrative limit exceeded";
        break;
      }
      case 12: {
        errorDescription = "Unavailable critical extension requested";
        break;
      }
      case 13: {
        errorDescription = "Confidentiality required";
        break;
      }
      case 14: {
        errorDescription = "SASL bind in progress";
        break;
      }
      case 16: {
        errorDescription = "No such attribute exists";
        break;
      }
      case 17: {
        errorDescription = "An undefined attribute type";
        break;
      }
      case 18: {
        errorDescription = "Inappropriate matching";
        break;
      }
      case 19: {
        errorDescription = "A constraint violation";
        break;
      }
      case 20: {
        errorDescription = "An attribute or value already in use";
        break;
      }
      case 21: {
        errorDescription = "An invalid attribute syntax";
        break;
      }
      case 32: {
        errorDescription = "No such object exists";
        break;
      }
      case 33: {
        errorDescription = "Alias problem";
        break;
      }
      case 34: {
        errorDescription = "An invalid DN syntax";
        break;
      }
      case 35: {
        errorDescription = "Is a leaf";
        break;
      }
      case 36: {
        errorDescription = " Alias dereferencing problem";
        break;
      }
      case 48: {
        errorDescription = "Inappropriate authentication";
        break;
      }
      case 49: {
        errorDescription = "Invalid credentials";
        break;
      }
      case 50: {
        errorDescription = "Insufficient access rights";
        break;
      }
      case 51: {
        errorDescription = "Busy";
        break;
      }
      case 52: {
        errorDescription = "Unavailable";
        break;
      }
      case 53: {
        errorDescription = "Unwilling to perform";
        break;
      }
      case 54: {
        errorDescription = "Loop detected";
        break;
      }
      case 64: {
        errorDescription = "Naming violation";
        break;
      }
      case 65: {
        errorDescription = "Object class violation";
        break;
      }
      case 66: {
        errorDescription = "Not allowed on non-leaf";
        break;
      }
      case 67: {
        errorDescription = "Not allowed on RDN";
        break;
      }
      case 68: {
        errorDescription = "Entry already exists";
        break;
      }
      case 69: {
        errorDescription = "Object class modifications prohibited";
        break;
      }
      case 71: {
        errorDescription = "Affects multiple DSAs";
        break;
      }
      case 80: {
        errorDescription = "Other";
        break;
      }
      case 999: {
        errorDescription = "General Exception";
        break;
      }
      default: {
        errorDescription = "Invalid code";
        break;
      }      
    }
    
    return errorDescription;
  }  
  
}

