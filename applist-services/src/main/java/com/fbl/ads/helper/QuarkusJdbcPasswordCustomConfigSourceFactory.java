package com.fbl.ads.helper;

import java.util.Collections;
import java.util.HashMap;

import org.apache.camel.component.jasypt.JasyptPropertiesParser;
import org.eclipse.microprofile.config.spi.ConfigSource;
import org.jboss.logging.Logger;

import io.smallrye.config.ConfigSourceContext;
import io.smallrye.config.ConfigSourceFactory;
import io.smallrye.config.ConfigValue;
import io.smallrye.config.PropertiesConfigSource;

public class QuarkusJdbcPasswordCustomConfigSourceFactory implements ConfigSourceFactory{
	private static final Logger LOG = Logger.getLogger(QuarkusJdbcPasswordCustomConfigSourceFactory.class);
	
    @Override
    public Iterable<ConfigSource> getConfigSources(final ConfigSourceContext context) {
        HashMap<String, String> customPropertiesMap = new HashMap<String, String>();
        
    	JasyptPropertiesParser propParser = new JasyptPropertiesParser();
       	String key = "FBL" + ((ConfigValue)context.getValue("application.crd.key")).getValue();
        propParser.setPassword(key);
        
        LOG.info("application.jdbc.usercrd:" + ((ConfigValue)context.getValue("application.jdbc.usercrd")).getValue());
        LOG.info("Key:" + key);
        
        ConfigValue passwordConfigValue = context.getValue("application.jdbc.usercrd");
        String password = propParser.parseProperty(key, passwordConfigValue.getValue(), null);
        customPropertiesMap.put("quarkus.datasource.password", password);
        
        return Collections.singletonList(new PropertiesConfigSource(customPropertiesMap, "quarkus.datasource.password.source", 290));
    }
	
}