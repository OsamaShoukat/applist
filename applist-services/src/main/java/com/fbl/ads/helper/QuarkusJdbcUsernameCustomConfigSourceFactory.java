package com.fbl.ads.helper;

import java.util.Collections;
import java.util.HashMap;

import org.eclipse.microprofile.config.spi.ConfigSource;
import org.jboss.logging.Logger;

import io.smallrye.config.ConfigSourceContext;
import io.smallrye.config.ConfigSourceFactory;
import io.smallrye.config.ConfigValue;
import io.smallrye.config.PropertiesConfigSource;

public class QuarkusJdbcUsernameCustomConfigSourceFactory implements ConfigSourceFactory{
	private static final Logger LOG = Logger.getLogger(QuarkusJdbcUsernameCustomConfigSourceFactory.class);
	
    @Override
    public Iterable<ConfigSource> getConfigSources(final ConfigSourceContext context) {
        HashMap<String, String> customPropertiesMap = new HashMap<String, String>();
        
        String username = ((ConfigValue)context.getValue("application.jdbc.user")).getValue();
        customPropertiesMap.put("quarkus.datasource.username", username);
        
        return Collections.singletonList(new PropertiesConfigSource(customPropertiesMap, "quarkus.datasource.username.source", 290));
    }
	

}