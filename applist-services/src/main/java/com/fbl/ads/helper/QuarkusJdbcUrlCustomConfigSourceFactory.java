package com.fbl.ads.helper;

import java.util.Collections;
import java.util.HashMap;

import org.eclipse.microprofile.config.spi.ConfigSource;
import org.jboss.logging.Logger;

import io.smallrye.config.ConfigSourceContext;
import io.smallrye.config.ConfigSourceFactory;
import io.smallrye.config.ConfigValue;
import io.smallrye.config.PropertiesConfigSource;

public class QuarkusJdbcUrlCustomConfigSourceFactory implements ConfigSourceFactory{
	private static final Logger LOG = Logger.getLogger(QuarkusJdbcUrlCustomConfigSourceFactory.class);
	
    @Override
    public Iterable<ConfigSource> getConfigSources(final ConfigSourceContext context) {
        HashMap<String, String> customPropertiesMap = new HashMap<String, String>();
        
        String host = ((ConfigValue)context.getValue("application.jdbc.host")).getValue();
        String port = ((ConfigValue)context.getValue("application.jdbc.port")).getValue();
        String serviceName = ((ConfigValue)context.getValue("application.jdbc.service.name")).getValue();
        String url = "jdbc:oracle:thin:@" + host + ":" + port + "/" + serviceName;
        customPropertiesMap.put("quarkus.datasource.jdbc.url", url);
        
        return Collections.singletonList(new PropertiesConfigSource(customPropertiesMap, "quarkus.datasource.jdbc.url.source", 290));
    }    
	
}