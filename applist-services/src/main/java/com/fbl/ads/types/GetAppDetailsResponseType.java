package com.fbl.ads.types;

public class GetAppDetailsResponseType {
  private ResponseHeaderType responseHeader;
  private GetAppResponseDetailsType responseDetails;
  
  public ResponseHeaderType getResponseHeader() {
    return responseHeader;
  }
  
  public void setResponseHeader(ResponseHeaderType responseHeader) {
    this.responseHeader = responseHeader;
  }
  
  public GetAppResponseDetailsType getResponseDetails() {
    return responseDetails;
  }
  
  public void setResponseDetails(GetAppResponseDetailsType responseDetails) {
    this.responseDetails = responseDetails;
  }
  
}
