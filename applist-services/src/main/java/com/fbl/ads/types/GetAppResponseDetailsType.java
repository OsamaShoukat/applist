package com.fbl.ads.types;

import java.util.List;

public class GetAppResponseDetailsType {
   List<AppType> appList;

  public List<AppType> getAppList() {
    return appList;
  }

  public void setAppList(List<AppType> appList) {
    this.appList = appList;
  }

}
