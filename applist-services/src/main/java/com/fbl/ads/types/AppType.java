package com.fbl.ads.types;

public class AppType {
  private String appId;
  private String appShortName;
  private String appName;
  private String appIconName;
  private String appResourcePath;
  private String appBundleName;
  private String appVersion;
  
  public String getAppId() {
    return appId;
  }
  public void setAppId(String appId) {
    this.appId = appId;
  }
  public String getAppShortName() {
    return appShortName;
  }
  public void setAppShortName(String appShortName) {
    this.appShortName = appShortName;
  }
  public String getAppName() {
    return appName;
  }
  public void setAppName(String appName) {
    this.appName = appName;
  }
  public String getAppIconName() {
    return appIconName;
  }
  public void setAppIconName(String appIconName) {
    this.appIconName = appIconName;
  }
  public String getAppResourcePath() {
    return appResourcePath;
  }
  public void setAppResourcePath(String appResourcePath) {
    this.appResourcePath = appResourcePath;
  }
  public String getAppBundleName() {
    return appBundleName;
  }
  public void setAppBundleName(String appBundleName) {
    this.appBundleName = appBundleName;
  }
  public String getAppVersion() {
    return appVersion;
  }
  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }
}
