package com.fbl.ads.types;


public class ResponseHeaderType {
  private String requestReferenceId;
  private String responseReferenceId;
  private String responseDateTime;
  private String responseCode;
  private String responseMessage;
  
  public String getRequestReferenceId() {
    return requestReferenceId;
  }
  
  public void setRequestReferenceId(String requestReferenceId) {
    this.requestReferenceId = requestReferenceId;
  }
  
  public String getResponseReferenceId() {
    return responseReferenceId;
  }
  
  public void setResponseReferenceId(String responseReferenceId) {
    this.responseReferenceId = responseReferenceId;
  }
  
  public String getResponseDateTime() {
    return responseDateTime;
  }
  
  public void setResponseDateTime(String responseDateTime) {
    this.responseDateTime = responseDateTime;
  }
  
  public String getResponseCode() {
    return responseCode;
  }
  
  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }
  
  public String getResponseMessage() {
    return responseMessage;
  }
  
  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }
}